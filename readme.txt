*****************************************************************************
** ChibiOS/RT port for ARM-Cortex-M4 STM32F407.                            **
*****************************************************************************

** TARGET **

The demo runs on an ST STM32F4-Discovery board.

** The Demo **
Lwip 2.0 integration with PPP plugin.
Using either PPPD (from linux) or a GPRS modem. For example SIM COM 900A (already 
tested with QUECTEL M95, Sierra Wireless HL6528, HL8518)

** Build Procedure **

The demo has been tested by using the free GNU ARM Toolchain on Launchpad [link](https://launchpad.net/gcc-arm-embedded). just modify 
the TRGT line in the makefile in order to use different GCC toolchains.

** Launching demo with Linux PPPD Server **

Allow IP forward so ChibiOS on Discovery board can access Internet through PPPD
sudo sysctl -w net.ipv4.ip_forward=1

*** Change IP addresses in ppp_option files ***
...
proxyarp

192.168.141.227:192.168.141.228
...
Using two IPs on the same subnet with your current ip address

*** Launch PPPd Server ***
sudo pppd file pppd_options

For debugging on Linux with Texane st-util: Launch GDB Server with ST-Link from Texane:
st-util -v99 -m

** Todo list **

Prepare lwip.mk like original ChibiOS's LWIP binding.
Reconnect in case of PPP connection error.

** Reference **
[1]: https://lists.nongnu.org/mailman/listinfo/lwip-devel "LWIP developer and user mailing list"
[2]: http://www.dalbert.net/?p=259 "David Albert wonderful article about ChibiOS, PPP new and Linux PPPD"
[3]: https://github.com/MarioViara/gprs "Mario Viara GPRS library for LWIP PPP-new"
[4]: https://github.com/wizhippo/stm32f4-chibios-lwip-pppos "Old lwip PPPoS, but fully functioning example"

** Notes **

Some files used by the demo are not part of ChibiOS/RT but are copyright of
ST Microelectronics and are licensed under a different license.
Also note that not all the files present in the ST library are distributed
with ChibiOS/RT, you can find the whole library on the ST web site:

                             http://www.st.com
